%global srcname pip
%global python_wheelname %{srcname}-%{version}-py3-none-any.whl
%global python_wheeldir %{_datadir}/python-wheels
%global _description \
pip is the package installer for Python. You can use pip to install packages from the Python Package Index and other indexes.
%global bashcompdir %(b=$(pkg-config --variable=completionsdir bash-completion 2>/dev/null); echo ${b:-%{_sysconfdir}/bash_completion.d})
Name:           python-%{srcname}
Version:        23.3.1
Release:        2
Summary:        A tool for installing and managing Python packages
License:        MIT and Python and ASL 2.0 and BSD and ISC and LGPLv2 and MPLv2.0 and (ASL 2.0 or BSD)
URL:            http://www.pip-installer.org
Source0:        %{pypi_source} 
Source1:        pip.loongarch.conf
BuildArch:      noarch
Patch1:         remove-existing-dist-only-if-path-conflicts.patch
Patch6000:      dummy-certifi.patch
Patch6001:      backport-CVE-2023-45803-Made-body-stripped-from-HTTP-requests.patch
Patch6002:      backport-CVE-2024-37891-Strip-Proxy-Authorization-header-on-redirects.patch

Source10: 	pip-allow-older-versions.patch

%description %{_description}

%package -n python%{python3_pkgversion}-%{srcname}
Summary:        %{summary}
BuildRequires:  python%{python3_pkgversion}-devel python%{python3_pkgversion}-setuptools bash-completion ca-certificates
Requires:       python%{python3_pkgversion}-setuptools ca-certificates
BuildRequires:  python%{python3_pkgversion}-wheel
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}
Provides:       python%{python3_pkgversion}dist(pip) = %{version}
Provides:       python%{python3_version}dist(pip) = %{version}
%description -n python%{python3_pkgversion}-%{srcname} %{_description}

%package_help

%package wheel
Summary:        The pip wheel
Requires:       ca-certificates

%description wheel
A Python wheel of pip to use with venv

%prep
%autosetup -n %{srcname}-%{version} -p1

# this goes together with Patch6000
rm src/pip/_vendor/certifi/*.pem
#sed -i '/\.pem$/d' src/pip.egg-info/SOURCES.txt
sed -i '/html_theme = "furo"/d' docs/html/conf.py

# Remove windows executable binaries
rm -v src/pip/_vendor/distlib/*.exe
sed -i '/\.exe/d' setup.py


%build
%py3_build_wheel

%install
%{__python3} dist/%{python_wheelname}/pip install \
    --root %{buildroot} \
    --no-deps \
    --no-cache-dir \
    --no-index \
    --ignore-installed \
    --find-links dist \
    'pip==%{version}'

%if %{with doc}
pushd docs/build/man
install -d %{buildroot}%{_mandir}/man1
for MAN in *1; do
install -pm0644 $MAN %{buildroot}%{_mandir}/man1/$MAN
for pip in "pip3" "pip-3" "pip%{python3_version}" "pip-%{python3_version}"; do
echo ".so $MAN" > %{buildroot}%{_mandir}/man1/${MAN/pip/$pip}
done
done
popd
%endif
 
# before we ln -s anything, we apply Source10 patch to all pips:
for PIP in %{buildroot}%{_bindir}/pip*; do
  patch -p1 --no-backup-if-mismatch $PIP < %{SOURCE10}
done
 
mkdir -p %{buildroot}%{bashcompdir}
PYTHONPATH=%{buildroot}%{python3_sitelib} \
    %{buildroot}%{_bindir}/pip completion --bash \
    > %{buildroot}%{bashcompdir}/pip3
 
# Make bash completion apply to all the 5 symlinks we install
sed -i -e "s/^\\(complete.*\\) pip\$/\\1 pip pip{,-}{3,%{python3_version}}/" \
    -e s/_pip_completion/_pip3_completion/ \
    %{buildroot}%{bashcompdir}/pip3
  
# Provide symlinks to executables to comply with Fedora guidelines for Python
ln -s ./pip%{python3_version} %{buildroot}%{_bindir}/pip-%{python3_version}
ln -s ./pip-%{python3_version} %{buildroot}%{_bindir}/pip-3
 
# Make sure the INSTALLER is not pip and remove RECORD, otherwise Patch2 won't work
# %%pyproject macros do this for all packages
echo rpm > %{buildroot}%{python3_sitelib}/pip-%{version}.dist-info/INSTALLER
rm %{buildroot}%{python3_sitelib}/pip-%{version}.dist-info/RECORD
 
mkdir -p %{buildroot}%{python_wheeldir}
install -p dist/%{python_wheelname} -t %{buildroot}%{python_wheeldir}

# Set default pip mirror via pip.conf
%ifarch loongarch64
install -D -m0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/pip.conf
%endif

%files -n python%{python3_pkgversion}-%{srcname}
%license LICENSE.txt
%ifarch loongarch64
%config(noreplace) %{_sysconfdir}/pip.conf
%endif
%{_bindir}/pip
%{_bindir}/pip3
%{_bindir}/pip-3*
%{_bindir}/pip%{python3_version}
%{python3_sitelib}/pip*
%dir %{bashcompdir}
%{bashcompdir}/pip3*
%dir %(dirname %{bashcompdir})

%files help
%doc README.rst

%files wheel
%license LICENSE.txt
%dir %{python_wheeldir}/
%{python_wheeldir}/%{python_wheelname}

%changelog
* Sat Jul 13 2024 yangyuan <yangyuan32@huawei.com> - 23.3.1-2
- Fix CVE-2023-45803 and CVE-2024-37891

* Wed Jan 31 2024 gengqihu <gengqihu2@h-partners.com> - 23.3.1-1
- update version to 23.3.1
- Fix parallel pip cache downloads causing crash (#12364)
- Upgrade urllib3 to 1.26.17 (#12343)
- Fix zsh completion script (#12173)
- Fix #12166
- Move the setuptools settings into pyproject.toml
- Use `-r=...` instead of `-r ...` for hg
- Drop isort and flake8 settings from setup.cfg
- Follow imports for more vendored dependencies
- Enable mypy's strict equality checks (#12209)
- Fixed argument name in docstring
- Allow truststore to not import on Python 3.9 and earlier
- Vendor truststore
- Fix formatting, combine numbers not strings!
- Remove uses of `utcnow` in non-vendored code (#12006)
- Fix issues raised in code review
- move test_download_metadata mock pypi index utilities to conftest.py
- Use strict optional checking in req_install.py (#11379)
- Fix 'force' remove file without write permissions
- Dropped unused attribute
- Fix Pytest --use-venv init
- Limit the double download fix to wheels
- Fix slowness on Python 3.11 when updating an existing large environment.
- Fix `pip completion --zsh`

* Fri Sep 08 2023 zhuofeng <zhuofeng2@huawei.com> - 23.1.2-3
- fix that pip install failed

* Sun Jul 30 2023 Funda Wang <fundawang@yeah.net> - 23.1.2-2
- Use local mirrors for pip

* Fri Jun  9 2023 dillon chen <dillon.chen@gmail.com> - 23.1.2-1
- upgrade version to 23.1.2

* Sat Jan 28 2023 zhuofeng<zhuofeng2@huawei.com> - 22.3.1-1                                          
- upgrade version to 22.3.1

* Wed Aug 03 2022 renhongxun <renhongxun@h-partners.com> - 21.3.1-3
- provides python3.10dist(pip) and python3dist(pip)

* Fri Jul 01 2022 dongyuzhen <dongyuzhen@h-partners.com> - 21.3.1-2
- Add patches that are deleted during the upgrade

* Mon Dec 20 2021 renhongxun<renhongxun@huawei.com> - 21.3.1-1
- upgrade version to 21.3.1

* Fri Nov 26 2021 shixuantong<shixuantong@huawei.com> - 20.3.3-5
- remove python3-pip from BuildRequires

* Mon Aug 23 2021 shixuantong<shixuantong@huawei.com> - 20.3.3-4
- delete bounded certificate

* Thu Aug 19 2021 shixuantong<shixuantong@huawei.com> - 20.3.3-3
- update dummy-certifi.patch

* Sat Jul 24 2021 shixuantong<shixuantong@huawei.com> - 20.3.3-2
- fix CVE-2021-3572

* Tue Feb 02 2021 shixuantong<shixuantong@huawei.com> - 20.3.3-1
- upgrade version to 20.3.3

* Wed Nov 4 2020 wangjie<wangjie294@huawei.com> -20.2.2-3
- Type:NA
- ID:NA
- SUG:NA
- DESC:remove python2

* Tue Sep 1 2020 wenzhanli<wenzhanli2@huawei.com> - 20.2.2-2
- add pip-allow-older-versions.patch

* Tue Aug 25 2020 wenzhanli<wenzhanli2@huawei.com> - 20.2.2-1
- update to 20.2.2 and add python2 require

* Tue Jul 28 2020 jinzhimin<jinzhimin2@huawei.com> - 20.1.1-1
- update to 20.1.1

* Thu Feb 20 2020 chengquan3<chengquan3@huawei.com> - 18.0-12
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: Remove incorrect provision of python-wheel

* Sat Nov 16 2019 huzhiyu <huzhiyu1@huawei.com> - 18.0-11
- fix ca-certificate bad requires

* Fri Oct 15 2019 lvying<lvying6@huawei.com> - 18.0-10
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: split same wheel file from python2-pip and python3-pip

* Sun Sep 29 2019 yefei <yefei25@huawei.com> - 18.0-9
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:  Synchronize  patches

* Sat Sep 28 2019 yefei <yefei25@huawei.com> - 18.0-8
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:  add bash completion and repair python2 interpreter path

* Wed Sep 25 2019 openEuler Buildteam <buildteam@openeuler.org> - 18.0-7
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:  Synchronize a patch

* Mon Sep 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 18.0-6
- Package init
